import tkinter as tk
import tkinter.messagebox as messagebox
from minecraft_launcher import launch_vm, stop_vm, start_vm, terminate_vm, get_instance_list, set_gui_mode
import gui_strings as mth
from os.path import dirname
from os import listdir, getcwd
import logging

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


class MineCraftApp:

    def __init__(self, master):
        try:
            set_gui_mode(True)
            master.title('Minecraft Server Control Panel')
            self.frame_top = tk.Frame(master)
            self.frame_top.grid(row=0, column=0, sticky=tk.N + tk.S + tk.E + tk.W)
            self.frame_bottom=tk.Frame(master)
            self.frame_bottom.grid(row=1, column=0, sticky=tk.N + tk.S + tk.E + tk.W)

            self.button_launch = tk.Button(self.frame_bottom, text=mth.LAUNCH_BTN_TEXT,
                                           command=self.click_launch_button)
            self.button_start = tk.Button(self.frame_bottom, text=mth.START_BTN_TEXT, command=self.click_start_button)
            self.buton_stop = tk.Button(self.frame_bottom, text=mth.STOP_BTN_TEXT, command=self.click_stop_button)
            self.button_terminate = tk.Button(self.frame_bottom, text=mth.TERMINATE_BTN_TEXT,
                                              command=self.click_terminate_button)
            self.listbox_config=tk.Listbox(self.frame_bottom, height=5)
            self.list_scroll=tk.Scrollbar(self.frame_bottom)
            self._get_config_file_list()
            self.listbox_config.grid(row=0, column=0, columnspan=2, sticky=tk.E + tk.W)
            self.list_scroll.grid(row=0, column=2, pady=4, sticky=tk.N + tk.S + tk.W)
            self.list_scroll.config(command=self.listbox_config.yview)
            self.listbox_config.config(yscrollcommand=self.list_scroll.set)
            self.button_launch.grid(row=5, column=0)
            self.button_start.grid(row=5, column=1)
            self.buton_stop.grid(row=5, column=2)
            self.button_terminate.grid(row=5, column=3)
            self._update_table()
            self.selected_row = None
        except Exception as exc:
            self.show_error_msg_box('Form initialize Error', str(exc))

    def _update_table(self):
        try:
            logger.info('Update table')
            instance_list = get_instance_list()
            logger.debug('Instance List: {0:s}'.format(str(instance_list)))
            rows_, columns_ = self.frame_top.grid_size()
            if rows_ == 0:
                logger.debug('Set up table header labels')
                for col in range(len(instance_list[0])):
                    label = tk.Label(self.frame_top, text=instance_list[0][col], borderwidth=2, relief='solid')
                    label.grid(row=0, column=col, sticky=[tk.E, tk.W])
            logger.debug('look for rows that need to be deleted or updated from the instance table')
            columns_, rows_ = self.frame_top.grid_size()
            for row_ in range(1, rows_):
                found=False
                table_instance_id=self.frame_top.grid_slaves(row=row_, column=0)[0]['text']
                for index in range(1, len(instance_list)):
                    if table_instance_id==instance_list[index][0]:
                        found=True
                        self._update_row(instance_list[index], row_)
                        del instance_list[index]
                        break
                if found==False:
                    logger.debug('Instance {0:s} in table but not in new list, call delete_row()'.format(table_instance_id))
                    self._delete_row(row_)
            logger.debug('rows left in the instance list need to be added')
            for index in range(1, len(instance_list)):
                self._add_row(instance_list[index])
        except Exception as exc:
            self.show_error_msg_box('Update Instance Table Error', str(exc))
        finally:
            self.frame_top.after(10000, self._update_table)

    def _delete_row(self, row_number):
        try:
            columns_, rows_ = self.frame_top.grid_size()
            for label in self.frame_top.grid_slaves(row=row_number):
                label.grid_forget()
                label.destroy()
            if row_number < rows_-1:
                for row_index in range(row_number+1, rows_):
                    for column_ in range(columns_):
                        label = self.frame_top.grid_slaves(row=row_index, column=column_)[0]
                        label.grid_forget()
                        label.grid(row=row_index-1, column=column_)
            self.frame_top.update()
        except Exception as del_exc:
            self.show_error_msg_box('Delete Instance Row Error', str(del_exc))

    def _update_row(self, value_tuple, row_number):
        try:
            for index in range(len(value_tuple)):
                label=self.frame_top.grid_slaves(row=row_number, column=index)[0]
                label.configure(text=value_tuple[index])
            self.frame_top.update()
        except Exception as ud_exc:
            self.show_error_msg_box("Error updating instance table", str(ud_exc))

    def _add_row(self, value_tuple):
        try:
            columns_, rows_ = self.frame_top.grid_size()
            for index in range(len(value_tuple)):
                labelx = tk.Label(self.frame_top, text=value_tuple[index], borderwidth=1, relief='solid')
                labelx.grid(row=rows_, column=index, sticky=[tk.E, tk.W])
                labelx.bind('<Button-1>', lambda event: self._select_row(rows_))
        except Exception as ad_exc:
            self.show_error_msg_box("Error adding Instance to table", str(ad_exc))

    def _get_row_by_key_value(self, value):
        try:
            columns_, rows_ = self.frame_top.grid_size()
            index=-1
            for x in range(rows_):
                label_text=self.frame_top.grid_slaves(row=x, column=0)[0]['text']
                if value==label_text:
                    index=x
                    break
            logger.debug('index of {0:s} is {1:d}'.format(value, index))
        except Exception as exc:
            self.show_error_msg_box("Error finding row to update", str(exc))
        finally:
            return index

    def _get_config_file_list(self, extension='cfg'):
        try:
            logger.info('load config file list')
            relevant_path=getcwd()
            included_extensions = ['.cfg', '.ini', '.conf']
            file_names = [fn for fn in listdir(relevant_path)
                          if any(fn.endswith(ext) for ext in included_extensions)]
            for item in file_names:
                self.listbox_config.insert(tk.END, item)
            if len(file_names) ==1:
                self.listbox_config.selection_set(0)
                logger.debug('config_list current selection {0:d}'.format(self.listbox_config.curselection()[0]))
        except Exception as exc:
            self.show_error_msg_box("Error getting config file list", str(exc))

    # def draw_table(self, target_frame):
    #     instance_list = get_instance_list()
    #     print(instance_list)
    #     #tk_labels_all = []
    #     #tk_labels_row = []
    #     for row_ in range(len(instance_list)):
    #         tk.Grid.rowconfigure(target_frame, row_, weight=1)
    #         for column_ in range(len(instance_list[row_])):
    #             tk.Grid.columnconfigure(target_frame, column_, weight=1)
    #             if row_ ==0:
    #               bw=2
    #             else:
    #               bw=1
    #             next_label = tk.Label(target_frame, text=instance_list[row_][column_],
    #                                        borderwidth=bw, relief="solid")
    #             next_label.grid(row=row_, column=column_, sticky=[tk.E, tk.W])
    #             next_label.bind('<Button-1>', lambda event: self.select_row(row_))
    #             #tk_labels_row[column_].grid(row=row_, column=column_)
    #         #tk_labels_all.append(tk_labels_row)
    #     target_frame.after(10000, self.draw_table, target_frame)

    def _get_instance_id(self):
        try:
            if self.selected_row is None:
                raise IndexError('No row selected in server table')
            instance_id=self.frame_top.grid_slaves(row=self.selected_row, column=0)[0]['text']
            logger.debug("got instance id {0:s}".format(instance_id))
            return instance_id
        except Exception as exc:
            self.show_error_msg_box("Error, is a row selected?", str(exc))

    def _get_value_by_name(self, key):
        try:
            logger.debug('get_value_by_name for key {0:s}'.format(key))
            selected_row = self.selected_row  #:TODO handle case when no row selected
            value = None
            _, columns_ = self.frame_top.grid_size()
            for column_ in range(1, columns_):
                if self.frame_top.grid_slaves(row=0, column=column_)[0]['text'] == key:
                    value = self.frame_top.grid_slaves(row=selected_row, column=column_)[0]['text']
                    break
        except Exception as exc:
            self.show_error_msg_box("Could not retrieve {0:s}".format(key), str(exc))
        finally:
            return value

    def _select_row(self, row_number):
        try:
            if row_number > 0 & row_number < self.frame_top.grid_size()[1]:
                for label in self.frame_top.grid_slaves(row=row_number):
                    label.configure(bg='blue')
            if (self.selected_row is not None) & (self.selected_row != row_number):
                for label in self.frame_top.grid_slaves(row=self.selected_row):
                    label.configure(bg='gray')
            self.selected_row = row_number
        except Exception as exc:
            self.show_error_msg_box("Error selecting row", str(exc))

    def click_launch_button(self):
        '''
        Event handler for user click of Launch VM button
        :return:
        '''
        logger.debug('launch button clicked handler')
        try:
            config_file=self.listbox_config.get(tk.ACTIVE)
        except IndexError as ie:
            logger.error('no config file selected, cannot launch')
            self.show_error_msg_box("Error", "no config file selected")
            return
        logger.info('launching vm with config file {0:s}'.format(config_file))
        try:
            launch_vm(config_file)
        except Exception as exc:
            self.show_error_msg_box("Error Launch VM", str(exc))

    def click_terminate_button(self):
        '''
        Event handler for user click of Terminate VM button
        :return:
        '''
        logger.debug('terminate button clicked handler')
        instance_id=self._get_instance_id()
        if instance_id is None:
            return
        try:
            terminate_vm(instance_id)
        except Exception as exc:
            self.show_error_msg_box("Error terminating VM", str(exc))

    def click_start_button(self):
        '''
        Event handler for user click of Start VM button
        :return:
        '''
        logger.debug('start button clicked handler')
        instance_id = self._get_instance_id()
        if instance_id is None:
            return
        state=self._get_value_by_name(mth.SERVER_STATE)
        logger.debug('state of selected instance is {0:s}'.format(state))
        try:
            if state=='stopped':
                logger.debug('starting instance {0:s}'.format(instance_id))
                start_vm(instance_id)
        except Exception as exc:
            self.show_error_msg_box("Error starting VM", str(exc))

    def click_stop_button(self):
        '''
        Event handler for user click of Stop VM button
        :return:
        '''
        logger.debug('stop button clicked handler')
        instance_id = self._get_instance_id()
        if instance_id is None:
            return
        state = self._get_value_by_name(mth.SERVER_STATE)
        logger.debug('state of selected instance is {0:s}'.format(state))
        if state == 'running':
            logger.debug('stopping instance {0:s}'.format(instance_id))
            try:
                stop_vm(instance_id)
            except Exception as exc:
                self.show_error_msg_box("Error stopping VM", str(exc))

    def show_error_msg_box(self, title, msg):
        '''
        Shows user a message box when an exception is raised.  This is a
        catch-all for unhandled exceptions. Should happen less and less
        as the code matures.
        :param title: Text which will appear as the caption of messagebox
        :param msg: The main text of the showerror messagebox
        :return:
        '''
        logger.debug('showing error message box {0:s}: {1:s}'.format(title, msg))
        messagebox.showerror(title, msg)


win = tk.Tk()
app = MineCraftApp(win)
win.mainloop()
