#!/usr/bin/env python3

__all__=['set_gui_mode', 'get_instance_list', 'launch_vm', 'start_vm', 'stop_vm', 'terminate_vm']

import boto3.ec2
from sys import argv
from os.path import isfile, expanduser
from os import linesep, sep as path_sep
from base64 import b64encode
from time import sleep
from configparser import ConfigParser
import logging
from gui_strings import *

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
ec2 = boto3.client('ec2')
has_gui=False


class MinecraftLauncherError(Exception):
    """Exception raised by minecraft_launcher.py"""

    def __init__(self, msg):
        self.msg = msg

    def __str__(self):
        return repr(self.msg)


def set_gui_mode(bool_gui):
    '''calling set_gui_mode(True) will cause exceptions to be re-thrown so that the GUI
    can put up a messagebox informing the user why a command did not work'''
    global has_gui
    has_gui=bool_gui


def _is_float(input_string):
    try:
        float(input_string)
        return True
    except (ValueError, TypeError):
        return False


def _is_int(input_string):
    try:
        int(input_string)
        return True
    except (ValueError, TypeError):
        return False


def _set_config(config_file='minecraft.cfg'):
    '''
    Finds the text file in .ini format whose name is passed in and
    validates all the entries.  File must be in the same directory as your python code.

    :param config_file: can be either the included minecraft.cfg or a file using
            minecraft.cfg as a template
    :return: returns a configparser object with validated values for launching a VM
    '''
    minecraft_config = ConfigParser()
    try:
        minecraft_config.read(config_file)
        if (minecraft_config.get('aws', '_AWS_SECURITY_GROUP') is None):
            raise Exception('minecraft_cfg._AWS_SECURITY_GROUP not found')

        if (minecraft_config.get('aws', '_AWS_BID_PRICE') is None):
            raise Exception('minecraft_cfg._AWS_BID_PRICE not found')
        elif (_is_float(minecraft_config.get('aws', '_AWS_BID_PRICE')) == True):
            if (_is_int(minecraft_config.get('aws', '_VM_AUTO_STOP_HOURS')) == False):
                minecraft_config.set('aws', '_VM_AUTO_STOP_HOURS', '0')  # TODO:should this be written back to file?
        elif (minecraft_config.get('aws', '_AWS_BID_PRICE') == 'on-demand'):
            pass
        else:
            raise Exception("minecraft_cfg._AWS_BID_PRICE should be a number in quotes or 'on-demand'\
                such as _AWS_BID_PRICE='0.10'")

        if (minecraft_config.get('aws', '_AWS_REGION') is None):
            raise Exception('minecraft_cfg._AWS_REGION not found')

        if (minecraft_config.get('aws', '_AWS_INSTANCE_TYPE') is None):
            raise Exception('minecraft_cfg._AWS_INSTANCE_TYPE not found')

        if (minecraft_config.get('aws', '_AWS_AMI_ID') is None):
            raise Exception('minecraft_cfg._AWS_AMI_ID not found')

        ssh_key = minecraft_config.get('aws', '_AWS_SSH_KEY')
        if (ssh_key is not None):
            local_key_path = '{0:s}{1:s}.ssh{1:s}{2:s}.pem'.format(expanduser('~'), path_sep,
                                                                   ssh_key)  # TODO: make this work on windows
            if (isfile(local_key_path) == False):
                raise Exception(
                    'your ssh private key file {0:s} was not found on the local machine. '.format(local_key_path))
        else:
            raise Exception('minecraft_cfg._AWS_SSH_KEY not found')

        user_data_template = minecraft_config.get('minecraft', '_VM_USER_DATA_TEMPLATE')
        if (user_data_template is not None):
            if (isfile(user_data_template) == False):
                raise Exception('User data template file {0:s} was not found'.format(user_data_template))
        else:
            raise Exception('minecraft_cfg._VM_USER_DATA_TEMPLATE not found')
        if (minecraft_config.get('minecraft', '_MINECRAFT_COMMMAND_LINE_ARGS') is None):
            minecraft_config.set('minecraft', '_MINECRAFT_COMMMAND_LINE_ARGS', '')
        return minecraft_config
    except Exception as e:
        logger.error("exception reading minecraft_cfg", exc_info=True)


def _set_user_data(minecraft_config):
    user_data_raw = minecraft_config.get('minecraft', '_VM_USER_DATA_TEMPLATE')
    command_line_args = minecraft_config.get('minecraft', '_MINECRAFT_COMMMAND_LINE_ARGS')

    try:
        insert_at = "args"
        user_data_template = minecraft_config.get('minecraft', '_VM_USER_DATA_TEMPLATE')
        logger.debug('opening user_data file {0:s}'.format(user_data_template))
        with open(user_data_template) as f:
            user_data_raw = f.read()
        if (command_line_args.strip()):
            jar_index = user_data_raw.find(insert_at)
            if (jar_index > 0):
                user_data_raw = user_data_raw[:jar_index] + ' ' + command_line_args
                logger.debug('user_data: {0:s}{1:s}'.format(linesep, user_data_raw))
        return b64encode(user_data_raw.encode()).decode()
    except Exception as e:
        logger.error("exception building user_data", exc_info=True)


def get_instance_list():
    '''
    Finds a list of instances in AWS that are servers launched by the tool.
    All instances that have a Tag with key 'MinecraftTag' are identified
    as Minecraft servers under the control of the minecraft_launcher.

    Relies on gui_strings.table_header_list

    :return: Returns a list of tuples. the first tuple consists of the column
            headers of attributes to be displayed. The remaining tuples are
            the critical attributes of each instance.  InstanceId must be the
            first item in the tuple. Did not use a dict because we wanted
            items in an enforceable order.
    '''
    logger.info("getting instance list")
    try:
        instance_list = [tuple(table_header_list),]
        raw_instance_list=[]
        reservation_list = ec2.describe_instances(Filters=
                                                    [{'Name': 'tag-key',
                                                      'Values': ['MinecraftTag']}])['Reservations']
        if len(reservation_list)>0:
            for reservation in reservation_list:
                raw_instance_list=reservation['Instances']
                for list_item in raw_instance_list:
                    instance_id = list_item.get('InstanceId')
                    state_name = list_item['State']['Name']
                    #ip_address = list_item['PublicIpAddress']
                    #hostname = list_item['PublicDnsName']
                    ip_address=list_item.get('PublicIpAddress', '--.--.--.--')
                    hostname=list_item.get('PublicDnsName','----------')
                    spot_or_sched_type = list_item['InstanceLifecycle']
                    server_name = 'not found'
                    tag_list = list_item['Tags']
                    for tag in tag_list:
                        if tag['Key'] == 'MinecraftTag':
                            server_name = tag['Value']
                    instance_list.append((instance_id, server_name, state_name, ip_address, hostname, spot_or_sched_type))

    except IndexError as e:
        logger.error("Index Error.  Looks like no instances have the Minecraft Tag")
    except Exception as e:
        logger.error("Exception during get_instance_list: {0:s}".format(e))
        if has_gui is True:
            template = "{0:s} Error getting instance list. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            raise MinecraftLauncherError(message)
    finally:
        return instance_list


def _get_ip(instance_id):
    '''
    retrieves the public IP Address of a server instance
    :param instance_id: the instance id of the desired server instance such as i-123456789
    :return: IPv4 Address (as string)
    '''
    ip_addr = ec2.describe_instances(InstanceIds=[instance_id])['Reservations'][0]['Instances'][0]['PublicIpAddress']
    return ip_addr


def _create_instance(minecraft_config, minecraft_tag='Minecraft Server'):
    '''
    Called by launch_vm.  Creates either a spot instance or 'on-demand' instance
    in AWS, passes the EC2 user data necessary to install Minecraft and run it.
    :param minecraft_config: an ini file with critical settings for the server
    :param minecraft_tag: A friendly name for the server.  Could be helpful if
    you launch multiple servers, say, with different Mods.
    :return: An EC2 list of instances (with one item).
    '''
    logger.info('create AWS instance')
    # ec2 = boto3.client('ec2')
    ami = minecraft_config.get('aws', '_AWS_AMI_ID')
    ssh_key = minecraft_config.get('aws', '_AWS_SSH_KEY')
    security_group = minecraft_config.get('aws', '_AWS_SECURITY_GROUP')
    instance_type = minecraft_config.get('aws', '_AWS_INSTANCE_TYPE')
    user_data = _set_user_data(minecraft_config)
    bid_price = minecraft_config.get('aws', '_AWS_BID_PRICE')
    block_duration_minutes = int(minecraft_config.get('aws', '_VM_AUTO_STOP_HOURS')) * 60
    if bid_price == 'on-demand':
        new_instance = ec2.run_instances(ImageId=ami, MinCount=1, MaxCount=1, KeyName=ssh_key,
                                            SecurityGroupIds=[security_group],
                                            UserData=user_data,
                                            InstanceType=instance_type,
                                            TagSpecifications = [
                                                {
                                                    'ResourceType': 'instance',
                                                    'Tags': [
                                                        {
                                                            'Key': 'MinecraftTag',
                                                            'Value': minecraft_tag,
                                                        },
                                                    ]
                                                },
                                            ],
                                        )
    else:
        new_instance = ec2.run_instances(ImageId=ami, MinCount=1, MaxCount=1, KeyName=ssh_key,
                                            SecurityGroupIds=[security_group],
                                            UserData=user_data,
                                            InstanceType=instance_type,
                                         TagSpecifications=[
                                             {
                                                 'ResourceType': 'instance',
                                                 'Tags': [
                                                     {
                                                         'Key': 'MinecraftTag',
                                                         'Value': minecraft_tag,
                                                     },
                                                 ]
                                             },
                                         ],
                                            InstanceMarketOptions={
                                                'MarketType': 'spot',
                                                'SpotOptions': {
                                                    'MaxPrice': bid_price,
                                                    'SpotInstanceType': 'one-time',
                                                    'BlockDurationMinutes': block_duration_minutes,
                                                    'InstanceInterruptionBehavior': 'terminate'
                                                }
                                            }
                                            )

    return new_instance


def create_spot_instance(minecraft_config, minecraft_tag='Minecraft Server'):
    '''deprecated'''
    logger.info('create spot AWS instance')
    # ec2 = boto3.client('ec2')
    ami = minecraft_config.get('aws', '_AWS_AMI_ID')
    ssh_key = minecraft_config.get('aws', '_AWS_SSH_KEY')
    security_group = minecraft_config.get('aws', '_AWS_SECURITY_GROUP')
    instance_type = minecraft_config.get('aws', '_AWS_INSTANCE_TYPE')
    bid_price = minecraft_config.get('aws', '_AWS_BID_PRICE')
    block_duration_minutes = int(minecraft_config.get('aws', '_VM_AUTO_STOP_HOURS')) * 60
    user_data = _set_user_data(minecraft_config)
    new_spot_request = ec2.request_spot_instances(
        SpotPrice=bid_price,
        BlockDurationMinutes=block_duration_minutes,
        InstanceCount=1,
        LaunchSpecification={
            'ImageId': ami,
            'KeyName': ssh_key,
            'SecurityGroupIds': [security_group],
            'UserData': b64encode(user_data.encode()).decode(),
            'InstanceType': instance_type,

        }
    )
    logger.debug(new_spot_request)
    request_id = new_spot_request['SpotInstanceRequests'][0]['SpotInstanceRequestId']
    while True:
        new_request = ec2.describe_spot_instance_requests(SpotInstanceRequestIds=[request_id])['SpotInstanceRequests'][
            0]
        request_state = new_request['State']
        if (request_state != 'open'):
            break

        logger.debug('spot request in open state, wait 10 seconds for active state')
        sleep(10)
    if (request_state != 'active'):
        raise Exception('spot request state {0:s}'.format(request_state))
    new_instance = new_request['InstanceId']
    logger.info('new instance id {0:s}'.format(new_instance))
    logger.info('the ip address of the new server is {0:s}'.format(_get_ip(new_instance)))
    return new_instance


def start_vm(instance_id):
    '''
    This function only works with on-demand instances.  If the instance is stopped
    a command will be sent to AWS to start it again.
    :param instance_id: string containing the InstanceId of the string to re-start
    :return: the EC2 State of the instance.  Most likely "starting". Does not
    wait for instance to complete the start-up process.
    '''
    try:
        logger.info('start_vm')
        # ec2 = boto3.client('ec2')
        instance_lifecycle = ec2.describe_instances(
            InstanceIds=[instance_id])['Reservations'][0]['Instances'][0]['InstanceLifecycle']
        if instance_lifecycle == 'scheduled':
            logger.info('starting scheduled instance {0:s}'.format(instance_id))
            res=ec2.start_instances(InstanceIds=[instance_id])
            state = res['StartingInstances'][0]['CurrentState']['Name']
            logger.debug(str(res))
            logger.info('Instance {0:s} is in state {1:s}'.format(instance_id, state))
        else:
            logger.debug('this is a spot instance, cannot start')
    except IndexError as inx_error:
        logger.error("Index error in start_vm command, {0:s} may not exist".format(instance_id), exc_info=True)
        if has_gui is True:
            template = "{0:s} Error while starting instance. Instance {1:s} may not exist. Arguments:\n{1!r}"
            message = template.format(type(inx_error).__name__, instance_id, inx_error.args)
            raise MinecraftLauncherError(message)
    except Exception as e:
        logger.error("exception while executing start_vm command", exc_info=True)
        if has_gui is True:
            template = "{0:s} Error while starting instance. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            raise MinecraftLauncherError(message)

def stop_vm(instance_id):
    '''
    This function only works with on-demand instances.  If the instance is running
    a command will be sent to AWS to start it again. For spot instances, user
    can only terminate.
    :param instance_id:
    :return:
    '''
    logger.info('stop_vm')
    try:
        instance_lifecycle = ec2.describe_instances(
            InstanceIds=[instance_id])['Reservations'][0]['Instances'][0]['InstanceLifecycle']
        if instance_lifecycle == 'scheduled':
            logger.info('stopping scheduled instance {0:s}'.format(instance_id))
            res = ec2.stop_instances(InstanceIds=[instance_id])
            state = res['StoppingInstances'][0]['CurrentState']['Name']
            logger.debug(res)
            logger.info('Instance {0:s} is in state {1:s}'.format(instance_id, state))
        else:
            logger.debug('this is a spot instance, cannot stop')
    except IndexError as inx_error:
        logger.error("Index error in stop_vm command, {0:s} may not exist".format(instance_id), exc_info=True)
        if has_gui is True:
            template = "{0:s} Error while stopping instance. Instance may not exist in AWS. Arguments:\n{1!r}"
            message = template.format(type(inx_error).__name__, inx_error.args)
            raise MinecraftLauncherError(message)
    except Exception as e:
        logger.error("exception while executing stop_vm command", exc_info=True)
        if has_gui is True:
            template = "{0:s} Error while stopping instance. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            raise MinecraftLauncherError(message)

def launch_vm(config_file='minecraft.cfg'):
    '''
    Creates/launches a new server instance in AWS.  This is the reason we are here!
    :param config_file: A text file in *.ini format with critical parameters
            for AWS and the server
    :return:
    '''
    logger.info('read and validate the config file {0:s}'.format(config_file))
    try:
        minecraft_config = _set_config(config_file)
    except Exception as e:
        logger.error("Error while reading config file", exc_info=True)
        if has_gui is True:
            template = "{0:s} Error while reading config file{1:s}. Arguments:\n{2!r}"
            message = template.format(type(e).__name__, config_file, e.args)
            raise MinecraftLauncherError(message)
    try:
        logger.info('launch_vm')
        _create_instance(minecraft_config)

    except Exception as lvm_e:
        logger.error("Error launching a vm in Amazon Web Services", exc_info=True)
        if has_gui is True:
            template = "{0:s} Error while launching new instance. Arguments:\n{1!r}"
            message = template.format(type(lvm_e).__name__, lvm_e.args)
            raise MinecraftLauncherError(message)


def terminate_vm(instance_id):
    '''
    Remove the server instance from AWS.  Stops the server and then deletes it completely.
    :param instance_id: The InstanceId of the server in AWS
    :return:
    '''
    logger.info('terminate_vm')
    try:
        res = ec2.terminate_instances(InstanceIds=[instance_id])
        state = res['TerminatingInstances'][0]['CurrentState']['Name']
        logger.debug('instance {0:s} in state {1:s} after terminate_instances call'.format(instance_id, state))
    except Exception as e:
        logger.error("exception terminating a vm in Amazon Web Services", exc_info=True)
        if has_gui is True:
            template = "{0:s} Error while terminating instance. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            raise MinecraftLauncherError(message)


def print_help():
    print('usage: >minecraft_launcher [command]')
    print("available commmands are:")
    print("\tstart_vm  - on-demand virtual machine only, unpause/restart")
    print("\tstop_vm  -  on-demand virtual machine will pause, spot instance will terminate")
    print("\tlaunch_vm  - creates a new virtual machine instance and starts server application")
    print("\tterminate_vm  - completely shuts down the vm, cannot be restarted")


# set_config()
if (__name__ == '__main__'):
    try:
        globals()[argv[1]]()
    except IndexError as e:
        print_help()
