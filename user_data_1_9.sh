#! /bin/bash
#apt-add-repository -y ppa:webupd8team/java
#apt-get update
#apt-get install oracle-java8-installer
yum update -y
cd /usr/local/bin
mkdir minecraft
chown ec2-user:ec2-user minecraft
cd minecraft
wget https://s3.amazonaws.com/Minecraft.Download/versions/1.9/minecraft_server.1.9.jar
ln -s minecraft_server.1.9.jar minecraft_server.jar
echo "eula=true" > eula.txt
java -jar minecraft_server.jar args
