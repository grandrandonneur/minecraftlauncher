#! /bin/bash
#apt-add-repository -y ppa:webupd8team/java
#apt-get update
#apt-get install oracle-java8-installer
yum -y update
yum -y install java-1.8.0
/usr/sbin/alternatives --set java /usr/lib/jvm/jre-1.8.0-openjdk.x86_64/bin/java
cd /usr/local/bin
mkdir minecraft
chown ec2-user:ec2-user minecraft
cd minecraft
wget https://launcher.mojang.com/v1/objects/3737db93722a9e39eeada7c27e7aca28b144ffa7/server.jar
ln -s server.jar minecraft_server.jar
echo "eula=true" > eula.txt
java -jar minecraft_server.jar args


