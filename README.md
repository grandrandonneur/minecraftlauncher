The Minecraft Launcher is a tool written in Python for launching Minecraft Servers in the Amazon Web Services Cloud using the EC2 service.

It assumes that the user has an amazon web services account, and has an ssh key in the file ~/.ssh/minecraft.pem.

User also needs to have AWS config and credentials at ~/.aws/config and ~/.aws/credentials, or declared as environment variables.  At time of first posting of the bitbucket repository, there is no tutorial.  Hopefully that is coming soon, but at this point, a basic familiarity with AWS is probably required.  There are tutorials on setting up Minecraft servers in AWS out there which will teach you what you need to know about having an AWS account.